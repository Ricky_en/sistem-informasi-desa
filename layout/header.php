<?php
require 'config/db_connect.php';
require 'config/function.libs.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Website Wonokeling</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.css">
  </head>
  <body>
    <header>
      <nav class="navbar">
        <div class="row">
          <div class="nav-brand">
            <a href="index.php">
              <img src="assets/images/logo39.png" style="width:50px;height:65px; margin-top:10px;">
              <h1 style="margin-left:15px; margin-top:=8px;color:orange;font-size:15px;text-transform:uppercase">
              <p style="font-size:13px !important;margin-top:12px;"> Desa Wonokeling </p>
              <p style="font-size:11px !important;margin-top:-15px"> Kecamatan Jatiyoso </p> 
              <p style="font-size:9px !important;margin-top:-13px"> Kabupaten Karanganyar </p></h1>
            </a>
          </div>
          <div class="nav-menu">
            <ul>
              <li><a href="index.php"><i class="fa fa-home"></i> HOME</a></li>
              <li><a href="berita.php"><i class="fa fa-newspaper"></i> BERITA</a></li>
                <li><a href="galeri.php"><i class="fa fa-images"></i> GALERI</a></li>
            </ul>
          </div>
          <div class="nav-sisa">
            <div class="kontak">
              <a href="kontak.php"><i class="fa fa-envelope"></i> Kontak Desa</a>
            </div>
          </div>
        </div>
      </nav>
    </header>
    <!-- <div class="clear"></div> -->
