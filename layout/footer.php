      </div>
    </div>

    <!-- Footer -->
    <div class="footer">
      <div class="rowflex">
        <div class="peta">
            <h1>Peta Desa Wonokeling</h1>
            <img width="300" height="190" src="assets/images/peta.png" alt="Foto Ir. Soekarno">
          </div>
        <div class="kutipan">
          <h1>Video Profil Desa</h1>
          <iframe width="300" height="190" src="https://www.youtube.com/embed/egIaRdGNWME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </div>
        <div class="clear"></div>
      </div>
      <div class="copyright-section">
        <p>Copyright © 2022, <a href="https://www.instagram.com/kknt39.wonokeling/" style="color:#92cf48;" target="_blank"><b>KKN TEMATIK 39</b></a> </p>
      </div>
  </div>
  </body>
</html>
